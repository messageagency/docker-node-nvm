# Use the circleci base as the parent image
FROM cimg/base:stable

# Switch to root user
USER root

RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash \
		&& source ~/.bashrc

RUN bash -c 'source $HOME/.nvm/nvm.sh && nvm install 10 && nvm use 10'
